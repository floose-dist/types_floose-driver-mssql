import {Framework, Utils} from "floose";
import Driver = Framework.Driver;
import Repository = Framework.Data.Repository;
import Model = Framework.Data.Model;
import SearchCriteria = Framework.Data.SearchCriteria;
import Join = Framework.Data.Join;
import Schema = Utils.Validation.Schema;

export declare enum MssqlQueryValueType {
    'VarChar' = "VarChar",
    'NVarChar' = "NVarChar",
    'Text' = "Text",
    'Int' = "Int",
    'BigInt' = "BigInt",
    'TinyInt' = "TinyInt",
    'SmallInt' = "SmallInt",
    'Bit' = "Bit",
    'Float' = "Float",
    'Numeric' = "Numeric",
    'Decimal' = "Decimal",
    'Real' = "Real",
    'Date' = "Date",
    'DateTime' = "DateTime",
    'DateTime2' = "DateTime2",
    'DateTimeOffset' = "DateTimeOffset",
    'SmallDateTime' = "SmallDateTime",
    'Time' = "Time",
    'UniqueIdentifier' = "UniqueIdentifier",
    'SmallMoney' = "SmallMoney",
    'Money' = "Money",
    'Binary' = "Binary",
    'VarBinary' = "VarBinary",
    'Image' = "Image",
    'Xml' = "Xml",
    'Char' = "Char",
    'NChar' = "NChar",
    'NText' = "NText",
    'TVP' = "TVP",
    'UDT' = "UDT",
    'Geography' = "Geography",
    'Geometry' = "Geometry",
    'Variant' = "Variant"
}

/**
 * UTILITIES
 */
export declare class MssqlUtility {
    /**
     * Convert a Date object to MsSql datetime string format using UTC timezone
     * @param date
     */
    static dateToMssqlDateTime(date: Date): string;
    /**
     * Convert string format 'Y-m-d H:i:s' in UTC timezone to a valid Date object in local timezone or do nothing
     * @param mssqlDateTime
     */
    static mssqlDateTimeToDate(mssqlDateTime: any): Date;
    /**
     * Convert a Date object to MsSql timestamp string format keeping local timezone
     * @param date
     */
    static dateToMssqlTimeStamp(date: Date): string;
    /**
     * Convert string format 'Y-m-d H:i:s' to a valid Date object in local timezone or do nothing
     * @param mssqlTimeStamp
     */
    static mssqlTimeStampToDate(mssqlTimeStamp: any): Date;
}

/**
 * DRIVER AND CONFIGURATION
 */
export declare interface MssqlDriverConfig {
    host: string;
    port: number;
    database: string;
    user: string;
    password: string;
}

export declare class MssqlDriver extends Driver {
    readonly configurationValidationSchema: Schema;
    init<T>(config: T): Promise<void>;
    query(sql: string, values?: {
        name: string;
        type?: {
            code: MssqlQueryValueType;
            args: {
                [name: string]: any;
            };
        };
        value: any;
    }[]): Promise<{
        rows?: { [key: string]: any }[];
        rowsAffected?: number;
    }[]>;
}

/**
 * MODEL AND REPOSITORY
 */
export declare abstract class AbstractMssqlModel extends Model {
}

export declare abstract class AbstractMssqlRepository<T extends AbstractMssqlModel> extends Repository<T> {
    protected _connection: string;
    constructor(connection: string);

    protected abstract _tableName(): string;
    protected abstract _tableIdentifier(): string;
    protected abstract _eventPrefix(): string;
    protected abstract _modelClass(): {new(): T};

    protected _find(searchCriteria: SearchCriteria, joinCriteria?: Join[]): Promise<T[]>;

    create(): T;
    get(identifier: any, forceReload?: boolean): Promise<T>;
    save(modelInterface: T): Promise<T>;
    delete(modelInterface: T | T[]): Promise<void>;
}